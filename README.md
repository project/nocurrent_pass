# No Current Password

This module disables the "current password" field that has been added to Drupal 8's user
edit form, at user/%/edit.

When you enable this module, the current password field will be removed by default.
To enable the password field again, go to admin/config/people/accounts and uncheck the
"Do no require current password" checkbox.

For a full description of the module, visit the
[project page](https://www.drupal.org/project/nocurrent_pass).

To submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/nocurrent_pass).


## Table of contents

- Requirements
- Installation
- Configuration
- Maintainers


## Requirements

This module requires no modules outside of Drupal core.


## Installation

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).


## Configuration

No configuration is needed.


## Maintainers

- Darrell Duane - [DarrellDuane](https://www.drupal.org/u/darrellduane)
- David Czinege - [david.czinege](https://www.drupal.org/u/davidczinege)
- Wes Jones - [earthday47](https://www.drupal.org/u/earthday47)
- Andrew Zahura - [shkiper](https://www.drupal.org/u/shkiper)
- Bohdan Artemchuk - [bohart](https://www.drupal.org/u/bohart)
- Viktor Holovachek - [AstonVictor](https://www.drupal.org/u/astonvictor)
